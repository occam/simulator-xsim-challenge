import argparse
import json
parser = argparse.ArgumentParser(description='Configuration options for this SST simulation.')

parser.add_argument('--input-program',
                    dest="input_program",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The program to run in the simulator')

parser.add_argument('--output-file',
                    dest="output_file",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The path to the output file')

parser.add_argument('--simulation-config',
                    dest="sim_config",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The json file with the simulation parameters')


# Used by the configuration.py file
class Configuration:

    args = None

    def __init__(self):
        self.args = parser.parse_args()

    # Parses the simulation configurationa and input files
    #  for the cpu parameters
    def get_cpu_params(self):
        with open(self.args.sim_config[0],'r') as f:
        	simulation_options = json.load(f)
        cpu_config = simulation_options.get("cpu",{})
        cpu_config["input_file"] = self.args.input_program[0]
        cpu_config["configuration_file"] = ""
        cpu_config["output_file"] = self.args.output_file[0]
        return cpu_config

    def __config_simpleMem(self, config, mem_config):
        for k,v in config.get("simpleMem",{}).items():
            mem_config["backend.%s"%(k)]=v
        return mem_config

    def __config_simpleDRAM(self, config, mem_config):
        for k,v in config.get("simpleDRAM",{}).items():
            mem_config["backend.%s"%(k)]=v
        return mem_config

    def get_memory_params(self):
        with open(self.args.sim_config[0],'r') as f:
            simulation_options = json.load(f)
        config=simulation_options.get("memory")
        if config:
            mem_config = {}
            mem_config["backend"] = config.get("backend")
            mem_config["backend.mem_size"] = config.get("backend.mem_size")
            mem_config["clock"] = config.get("clock")
            if mem_config["backend"] == "memHierarchy.simpleMem":
                mem_config=self.__config_simpleMem(config, mem_config)
            elif mem_config["backend"] == "memHierarchy.simpleDRAM":
                mem_config=self.__config_simpleDRAM(config, mem_config)
        return mem_config

    def get_link_latency(self):
        return "300ps"
    # Uncomment this
    def has_cache(self):
        with open(self.args.sim_config[0],'r') as f:
        	simulation_options = json.load(f)
        l1_config = simulation_options.get("l1cache",{})
        if(l1_config == {}):
            return False
        return True

    def has_l2_cache(self):
        with open(self.args.sim_config[0],'r') as f:
            simulation_options = json.load(f)
        return simulation_options.get("l2_enabled")


    def get_l1_params(self):
        with open(self.args.sim_config[0],'r') as f:
            simulation_options = json.load(f)
        l1_config = simulation_options.get("l1cache",{})
        if(l1_config != {}):
            l1_config["L1"]=1
        return l1_config

    def get_cache_params(self, cache_name):
        with open(self.args.sim_config[0],'r') as f:
            simulation_options = json.load(f)
        l1_config = simulation_options.get("l1cache",{})
        cache_config = simulation_options.get(cache_name,{})
        if(cache_config != {}):
            cache_config["L1"]=0
            cache_config["cache_line_size"]=l1_config["cache_line_size"]
        return cache_config





class OutputGenerator:

    experiments=[]

    def __output_cpu(self, cpu_output, simulation_configuration):
        cpu_output["clock_frequency"] = simulation_configuration.get("cpu").get("clock_frequency")
        cpu_output = {
          "cpu":cpu_output
        }
        return cpu_output

    def __output_memory(self, simulation_configuration):
        memory_output = {
            "memory": {
                "clock": simulation_configuration.get("memory").get("clock")
            }
        }
        return memory_output

    def __get_plots(self):
        plots={}
        ################################################
        #  Memory frequency vs number of cpu cycles    #
        ################################################
        mem_freq_vs_cpu_cycles={}
        for experiment in self.experiments:
            # Get experiment results and inputs
            cpu_output = experiment.get("cpu_output")
            sst_output = experiment.get("sst_output")
            simulation_configuration = experiment.get("simulation_configuration")
            input_program = experiment.get("input_program")
            # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
            data = mem_freq_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
            # X-axis contains the memory frequencies
            data.get("x_data").append(simulation_configuration.get("memory").get("clock"))
            # Y-axis contains the number of cpu cycles executed
            data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))
            # Update plot line with new data
            mem_freq_vs_cpu_cycles.update({input_program:data})
        plots["mem_freq_vs_cpu_cycles"] = []
        for k,data in mem_freq_vs_cpu_cycles.items():
            plots["mem_freq_vs_cpu_cycles"].append({
                "x_data": data.get("x_data"),
                "y_data": data.get("y_data"),
                "legend": data.get("legend")
            })
        ################################################
        # Cache block size vs number of l1 cache misses#
        #         only if there is an sst output       #
        ################################################
        if sst_output != None:
            blocksize_vs_l1_cachemisses={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = blocksize_vs_l1_cachemisses.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # Look for CacheMisses statistics
                cache_misses = sst_output.get("l1cache",{}).get("CacheMisses", None)
                if cache_misses != None:
                    # X-axis contains the cache block sizes
                    data.get("x_data").append(simulation_configuration.get("l1cache").get("cache_line_size"))
                    # Y-axis contains the total number of cache misses found in column 8 of the row
                    cache_misses = int(cache_misses)
                    data.get("y_data").append(cache_misses)
                    # Update plot line with new data
                    blocksize_vs_l1_cachemisses.update({input_program:data})
            plots["blocksize_vs_l1_cachemisses"] = []
            for k,data in blocksize_vs_l1_cachemisses.items():
                plots["blocksize_vs_l1_cachemisses"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        # Cache block size vs number of l2 cache misses#
        #         only if there is an sst output       #
        ################################################
        if sst_output != None:
            blocksize_vs_l2_cachemisses={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = blocksize_vs_l2_cachemisses.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # Look for CacheMisses statistics
                cache_misses = sst_output.get("l2cache",{}).get("CacheMisses", None)
                if cache_misses != None:
                    # X-axis contains the cache block sizes
                    data.get("x_data").append(simulation_configuration.get("l1cache").get("cache_line_size"))
                    # Y-axis contains the total number of cache misses found in column 8 of the row
                    cache_misses = int(cache_misses)
                    data.get("y_data").append(cache_misses)
                    # Update plot line with new data
                    blocksize_vs_l2_cachemisses.update({input_program:data})
            plots["blocksize_vs_l2_cachemisses"] = []
            for k,data in blocksize_vs_l2_cachemisses.items():
                plots["blocksize_vs_l2_cachemisses"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        #  Cache block size vs number of cpu cycles    #
        ################################################
        if simulation_configuration.get("l1cache") != None:
            blocksize_vs_cpu_cycles={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = blocksize_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # X-axis contains the cache block sizes
                data.get("x_data").append(simulation_configuration.get("l1cache").get("cache_line_size"))
                # Y-axis contains the number of cpu cycles executed
                data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))

                blocksize_vs_cpu_cycles.update({input_program:data})
            plots["blocksize_vs_cpu_cycles"] = []
            for k,data in blocksize_vs_cpu_cycles.items():
                plots["blocksize_vs_cpu_cycles"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        # Cache associativity vs number of cpu cycles  #
        ################################################
        if simulation_configuration.get("l1cache") != None:
            l1_associativity_vs_cpu_cycles={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = l1_associativity_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # X-axis contains the cache block sizes
                data.get("x_data").append(simulation_configuration.get("l1cache").get("associativity"))
                # Y-axis contains the number of cpu cycles executed
                data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))

                l1_associativity_vs_cpu_cycles.update({input_program:data})
            plots["l1_associativity_vs_cpu_cycles"] = []
            for k,data in l1_associativity_vs_cpu_cycles.items():
                plots["l1_associativity_vs_cpu_cycles"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        # Cache associativity vs number of cpu cycles  #
        ################################################
        if simulation_configuration.get("l2cache") != None:
            l2_associativity_vs_cpu_cycles={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = l2_associativity_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # X-axis contains the cache block sizes
                data.get("x_data").append(simulation_configuration.get("l2cache").get("associativity"))
                # Y-axis contains the number of cpu cycles executed
                data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))

                l2_associativity_vs_cpu_cycles.update({input_program:data})
            plots["l2_associativity_vs_cpu_cycles"] = []
            for k,data in l2_associativity_vs_cpu_cycles.items():
                plots["l2_associativity_vs_cpu_cycles"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        # number of levels of cache vs number of cycles#
        ################################################
        if simulation_configuration.get("l1cache") != None:
            cache_levels_vs_cpu_cycles={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = cache_levels_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # X-axis contains the cache block sizes
                print(simulation_configuration.get("l2_enabled"))
                if simulation_configuration.get("l2_enabled", False):
                    n_levels=2
                else:
                    n_levels=1
                data.get("x_data").append(n_levels)
                # Y-axis contains the number of cpu cycles executed
                data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))

                cache_levels_vs_cpu_cycles.update({input_program:data})
            plots["cache_levels_vs_cpu_cycles"] = []
            for k,data in cache_levels_vs_cpu_cycles.items():
                plots["cache_levels_vs_cpu_cycles"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        ################################################
        # total cache size vs number of cycles#
        ################################################
        if simulation_configuration.get("l1cache") != None:
            total_cache_size_vs_cpu_cycles={}
            for experiment in self.experiments:
                # Get experiment results and inputs
                cpu_output = experiment.get("cpu_output")
                sst_output = experiment.get("sst_output")
                simulation_configuration = experiment.get("simulation_configuration")
                input_program = experiment.get("input_program")
                # Get the data for this input program, otherwise create a new plot line -- legend contains the input program name
                data = total_cache_size_vs_cpu_cycles.get(input_program, {"x_data":[], "y_data":[], "legend":input_program})
                # X-axis contains the cache total size
                cache_size = "L1=%s"%(simulation_configuration.get("l1cache").get("cache_size"))
                if simulation_configuration.get("l2_enabled", False):
                    cache_size = "%s, L2=%s"%(cache_size, simulation_configuration.get("l2cache").get("cache_size"))
                data.get("x_data").append(cache_size)
                # Y-axis contains the number of cpu cycles executed
                data.get("y_data").append(cpu_output.get("stats")[0].get("cycles"))

                total_cache_size_vs_cpu_cycles.update({input_program:data})
            plots["total_cache_size_vs_cpu_cycles"] = []
            for k,data in total_cache_size_vs_cpu_cycles.items():
                plots["total_cache_size_vs_cpu_cycles"].append({
                    "x_data": data.get("x_data"),
                    "y_data": data.get("y_data"),
                    "legend": data.get("legend")
                })
        return plots

    def __output_cache(self, cache_name, sst_output, simulation_configuration):
        cache_output = {cache_name:{}}
        has_cache = False
        # Search across the SST output according to the statistics in:
        #   sst-info memHierarchy.Cache
        cache_misses = sst_output.get(cache_name,{}).get("CacheMisses", None)
        if cache_misses != None:
            cache_output.get(cache_name)["cache_misses"]= cache_misses
            has_cache = True
        # Search across the configuration file provided by OCCAM according to the configuration schema
        l1cache_configuration = simulation_configuration.get(cache_name, None)
        if l1cache_configuration != None:
            # Copy all configuration options to the output
            for k,v in l1cache_configuration.items():
                cache_output.get(cache_name)[k]=str(v)
            has_cache = True
        if not has_cache:
            cache_output = None
        return cache_output

    # This function is called by the generate_output.py file to set the output of a single experiment
    def add_experiment(self, cpu_output, sst_output, simulation_configuration, input_program):
        sst_parsed_output = None
        if sst_output != None:
            sst_parsed_output = {}
            for row in sst_output:
                if row != sst_output[0]:
                    component_name =  row[0].strip()
                    statistic_name =  row[1].strip()
                    statistic_total = row[8].strip()
                    component = sst_parsed_output.get(component_name, {})
                    component[statistic_name] = statistic_total
                    sst_parsed_output.update({component_name:component})
        self.experiments.append({
            "cpu_output": cpu_output,
            "sst_output": sst_parsed_output,
            "simulation_configuration": simulation_configuration,
            "input_program": input_program
        })

    # This function is called by the generate_output.py file to get the output
    def get_output(self):
        final_output = {"experiments":[]}
        for experiment in self.experiments:
            # Get experiment results and inputs
            cpu_output = experiment.get("cpu_output")
            sst_output = experiment.get("sst_output")
            simulation_configuration = experiment.get("simulation_configuration")
            input_program = experiment.get("input_program")

            cpu = self.__output_cpu(cpu_output, simulation_configuration)
            memory = self.__output_memory(simulation_configuration)
            l1cache = None
            l2cache = None
            if sst_output != None:
                l1cache = self.__output_cache("l1cache", sst_output, simulation_configuration)
                l2cache = self.__output_cache("l2cache", sst_output, simulation_configuration)
            output = {}
            output.update(cpu)
            output.update(memory)
            output.update({"input_program": input_program})
            if l1cache != None:
                output.update(l1cache)
            if l2cache != None:
                output.update(l2cache)
            final_output["experiments"].append(output)
        # Get data to be plotted
        plots = self.__get_plots()
        final_output.update({"plots":plots})
        return final_output


def input_parser():
    return Configuration()

def output_parser():
    return OutputGenerator()
