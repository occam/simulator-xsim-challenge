import os
import json
from occam import Occam
#Get information about object
object = Occam.load()
#Gather paths
scripts_path = os.path.dirname(__file__)
job_path = os.getcwd()
object_path = "/occam/%s-%s"%(object.id(),object.revision())

#Set variables
runner_script=os.path.join(object_path, "runner.py")
build_folder=os.path.join(object_path,"build")
input_files=[]
inputs = object.inputs("program/XSim")
for input in inputs:
    for file in input.files():
        input_files.append(file)
if len(input_files)==0:
    input_files.append(os.path.join(object_path,"default_program.m"))




simulation=os.path.join(object_path, "simulation.py")
simulation_configuration=os.path.join(object_path,"default_simulation.json")
simulation_configuration = object.configuration_file("Simulation configuration")
output_file="statistics.json"
output_file = object.output_path("application/json","output_dir","statistics.json")
#Setup run command
run_sim_command = [
        "python ", runner_script,
        " --simulation ", simulation,
        " --build-folder ", build_folder,
        " --simulation-config ", simulation_configuration,
        " --output-file ", output_file,
        " --input-program "
]
for input in input_files:
    run_sim_command.append("\"%s\""%input)

command = ' '.join(run_sim_command)
#pass run command to occam
Occam.report(command)
