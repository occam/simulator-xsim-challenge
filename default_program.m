# September 26, 2016
# this program sums the numbers 0 to 186.
# Fill memory[0..10] numbers from 0 to 186
#
#
0x81BB    #0  liz $r1, 187       |	int stop(r1)=187;
0x8201    #1  liz $r2, 1         |	int step(r2)=1;
0x8000    #2  liz $r0, 0         |	for(int i(r0)=0; (r1-r0)==0;r0=r0+r2)
0x0B20    #3  sub $r3, $r1, $r0  |
0xBB08    #4  bz  $r3, 8         |	{
0x4800    #5  sw  $r0, $r0       |		mem[i]=i;
0x0008    #6  add $r0, $r0, $r2  |	}
0xC003    #7  j 3                |
# Sum all values                 |
0x8700    #8  liz $r7, 0         |	int acc(r7)=0;
0x81BB    #9  liz $r1, 187       |	int stop(r1)=187;
0x8201    #10 liz $r2, 1         |	int step(r2)=1;
0x8000    #11 liz $r0, 0         |	for(int i(r0)=0;(r1-r0)==0;r0=r0+r2)
0x0B20    #12 sub $r3, $r1, $r0  |
0xBB12    #13 bz  $r3, 18        |	{
0x4300    #14 lw  $r3, $r0       |		int num(r3)=mem[i];
0x07EC    #15 add $r7, $r7, $r3  |		r7=r7+r3;
0x0008    #16 add $r0, $r0, $r2  |	}
0xC00C    #17 j 12               |
# Display result                 |
0x70E0    #18 put $r7            |	std::cout<<acc<<std::endl;
0x6800    #19 halt               |	return;
