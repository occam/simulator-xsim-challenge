import os
import sys
import json
import argparse
import json
from subprocess import call

parser = argparse.ArgumentParser(description='Configuration options for this simulation.')

parser.add_argument('--simulation',
                    dest="simulation",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The SST simulation file')

parser.add_argument('--build-folder',
                    dest="build",
                    nargs=1,
                    required=True,
                    action='store',
                    help='Location of the XSim library')

parser.add_argument('--input-program',
                    dest="input_program",
                    nargs="*",
                    required=True,
                    action='store',
                    help='The program to run in the simulator')


parser.add_argument('--simulation-config',
                    dest="sim_config",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The json file with the simulation parameters')

parser.add_argument('--output-file',
                    dest="output_file",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The path to the output file')


scripts_path    = os.path.dirname(os.path.abspath(__file__))

################################################
#   This function generates runs a simulation  #
#     for each input file and for each memory  #
#     frequency specified                      #
################################################
def run(simulation="", simulation_configuration_file="", build_folder="", input_files=[], output_file=""):
    ## Load the simulation configuration
    with open(simulation_configuration_file) as json_file:
        simulation_configuration=json.load(json_file)

    # Command to be executed
    commands=[]
    # Split the list of memory clocks to be simulated
    clocks=simulation_configuration.get("memory").get("clock").split(',')
    for i in range(len(clocks)):
        clocks[i]=clocks[i].strip()

    ## For each input file, run the simulation with it.
    #    For each memory frequency in the list, create a copy of the
    #    configuration file for each of those frequencies.
    run_number=0
    for input_file in input_files:
        for clock_frequency in clocks:
            # Create a simulation configuration using each clock frequency specified

            simulation_configuration["memory"]["clock"]=clock_frequency
            # Put the configuration in a temporary file to be used by the simulation

            sub_simulation_configuration = "%s/sim_config%d.json"%(scripts_path,run_number)
            with open(sub_simulation_configuration, "w") as sim_file:
                sim_file.write(json.dumps(simulation_configuration, indent=4, separators=(',', ': ')))
            # File to temporarily store the output of the simulation
            sub_output_path = "%s/output%d.json"%(scripts_path,run_number)
            # Create command to run the simulator
            run_sim_command = [
                "function run%d(){ "%(run_number),
                "sst ", simulation,
                " --add-lib-path=", build_folder,
                " --model-options '",
                " --input-program=", input_file,
                " --simulation-config=",sub_simulation_configuration,
                " --output-file=",sub_output_path,
                "';};",
                "run%d;"%(run_number),
                "if [ -e sst-cache-stats.csv ]; then mv sst-cache-stats.csv sst-cache-stats%d.csv; fi;"%(run_number)
            ]
            run_number=run_number+1
            # Append simulation command to the list of commands
            commands.append(''.join(run_sim_command))

    # Read output of simulations and create the actual output file
    output_generator_path = os.path.join(scripts_path, "generate_output.py")
    generator_command = "python %s --name \'%s\' --output %s --clocks " % (output_generator_path, simulation_configuration["name"], output_file)
    for clock in clocks:
        generator_command = generator_command + "%s "%(clock)
    generator_command = generator_command + " --files "
    for file in input_files:
        generator_command = generator_command + "%s "%(file)

    commands.append(generator_command)

    commands=''.join(commands)
    return commands



args = parser.parse_args()
input_files = []
for input in args.input_program:
    input_files.append(input)
command=run(args.simulation[0], args.sim_config[0], args.build[0], input_files, args.output_file[0])
call(["bash", "-i", "-c", "cd %s;%s"%(scripts_path,command)])
