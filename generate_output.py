import os
import json
import argparse
import csv
import utils

parser = argparse.ArgumentParser()

parser.add_argument('--clocks',
                    dest="clocks",
                    nargs="*",
                    required=True,
                    action='store')
parser.add_argument('--output',
                    dest="output_path",
                    required=True,
                    action='store')
parser.add_argument('--name',
                    dest="name",
                    required=True,
                    action='store')

parser.add_argument('--files',
                    dest="inputs",
                    nargs="*",
                    required=True,
                    action='store')

args=parser.parse_args()

scripts_path    = os.path.dirname(os.path.abspath(__file__))

# File to create with the simulation output
output_path = args.output_path

# Memory clock frequencies and input files used in the experiment
clocks = args.clocks
n_clocks = len(clocks)
inputs = args.inputs
n_inputs = len(inputs)
n_outputs = n_inputs*n_clocks


# Output of the simulations
occam_output = {
    "name": args.name
}

output_parser = utils.output_parser()

for inp_idx in range(0, n_inputs):
    for clock_idx in range(0, n_clocks):
        i = inp_idx*n_clocks+clock_idx
        output_path_i = "output%d.json"%(i)
        config_path_i = "sim_config%d.json"%(i)
        csv_file_i    = "sst-cache-stats%d.csv"%(i)
        with open(output_path_i) as sim_output_file:
            sim_output = json.load(sim_output_file)

        with open(config_path_i) as sim_config_file:
            sim_config = json.load(sim_config_file)

        if os.path.exists(csv_file_i):
            with open(csv_file_i) as sst_output_file:
                sst_output_csv = csv.reader(
                    sst_output_file,
                    delimiter=str(",")
                )
                sst_output = []
                for row in sst_output_csv:
                    sst_output.append(row)

        else:
            sst_output = None
        program_name = os.path.basename(inputs[inp_idx])
        output_parser.add_experiment(sim_output, sst_output, sim_config, program_name)

output_i = output_parser.get_output()
occam_output.update(output_i)

with open(output_path, "w+") as occam_output_file:
    occam_output_file.write(json.dumps(occam_output, indent=4, separators=(',', ': ')))

