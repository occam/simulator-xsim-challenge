import sst
import argparse
import json


def parse_config(configuration):
    output = {}
    for key in configuration:
        value = configuration[key]["value"]
        if isinstance(value, dict):
            output[key] = parse_config(value)
        else:
            unit = configuration[key].get("unit", None)
            if unit != None:
                value = "%d%s"%(value, unit)
            output[key] = value
    return output

parser = argparse.ArgumentParser(description='Configuration options for this SST simulation.')

parser.add_argument('--input-program',
                    dest="input_program",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The program to run in the simulator')

parser.add_argument('--output-file',
                    dest="output_file",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The path to the output file')

parser.add_argument('--cpu-latencies',
                    dest="cpu_latencies",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The json file with the cpu latencies')

parser.add_argument('--simulation-config',
                    dest="sim_config",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The json file with the simulation parameters')


args=parser.parse_args()

input_program=args.input_program[0]
output_file=args.output_file[0]
cpu_latencies=args.cpu_latencies[0]
sim_config=args.sim_config[0]

with open(sim_config,'r') as f:
	simulation_options = parse_config(json.load(f))
print (json.dumps(simulation_options, indent=4, separators=(',', ': ')))

cache_link_latency = "300ps"

core = sst.Component("simpleMIPS","simpleMIPS.mips_core")
print(simulation_options.get("cpu",{}))
core.addParams(simulation_options.get("cpu",{}))
core.addParams({
    "input_file":input_program,
	"configuration_file":cpu_latencies,
	"output_file":output_file,
})

memory = sst.Component("data_memory", "memHierarchy.MemController")
mem_config=simulation_options.get("memory")
if mem_config:
	backend=mem_config.get("backend")
	memsize=mem_config.get("backend.mem_size")
	clock=mem_config.get("clock")
	print(clock)
	memory.addParams(
	{
	    'backend':				backend,
		'backend.mem_size':		memsize,
		'clock':				clock,
	})
	if backend == "memHierarchy.simpleMem":
		temp=mem_config.get("simple_mem",{})
		for k,v in temp:
			backend_config["backend.%s"%(k)]=v
		memory.addParams(backend_config)


cpu_data_memory_link = sst.Link("cpu_data_memory_link")
cpu_data_memory_link.connect((core, "data_memory_link", cache_link_latency), (memory, "direct_link", cache_link_latency))


# Enable SST Statistics Outputs for this simulation
statLevel = 16
statFile = "stats.csv"
sst.setStatisticLoadLevel(statLevel)
sst.enableAllStatisticsForAllComponents({"type":"sst.AccumulatorStatistic"})

sst.setStatisticOutput("sst.statOutputCSV")
sst.setStatisticOutputOptions( {
    "filepath"  : statFile,
	"separator" : ", "
	} )
