#include <sst/core/sst_config.h>
#include <simpleMIPS/mips_core.hpp>
#include <sstream>
#include <sst/core/interfaces/simpleMem.h>
#include <sst/core/interfaces/stringEvent.h>
#include <sst/core/simulation.h>


#define print_request(req_to_print) \
{\
	std::cout<<\
	(req_to_print->cmd==Interfaces::SimpleMem::Request::Command::Write?"Write":\
	(req_to_print->cmd==Interfaces::SimpleMem::Request::Command::Read?"Read":\
	(req_to_print->cmd==Interfaces::SimpleMem::Request::Command::ReadResp?"ReadResp":\
	(req_to_print->cmd==Interfaces::SimpleMem::Request::Command::WriteResp?"WriteResp":"Some flush"\
	))))<<std::endl<<\
	"\tTo Address "<<req_to_print->addr<<std::endl<<\
	"\tSize "<<req_to_print->size<<std::endl<<\
	"\tId "<<req_to_print->id<<std::endl;\
	}

namespace SST
{
namespace simpleMIPS
{



mips_core::mips_core(SST::ComponentId_t id, SST::Params& params):
	Component(id)
{

	verbose = (uint32_t) params.find<uint32_t>("verbose", verbose);

	output = new SST::Output("mips_core[@t:@l]: ", verbose, 0, SST::Output::STDOUT);

	//***********************************************************************************************
	//*	Some of the parameters may not be avaiable here, e.g. if --run-mode is "init"				*
	//*		As a consequence, this section shall not throw any errors. Instead use setup section	*
	//***********************************************************************************************

	clock_frequency=params.find<std::string>("clock_frequency",clock_frequency);


	bool success;
	output->verbose(CALL_INFO, 1, 0, "Configuring cache connection...\n");

	data_memory_link = dynamic_cast<SST::Interfaces::SimpleMem*>(Super::loadModuleWithComponent("memHierarchy.memInterface", this, params));
	SST::Interfaces::SimpleMem::Handler<mips_core> *data_handler=
			new SST::Interfaces::SimpleMem::Handler<mips_core>(this,&mips_core::memory_callback);
	if(!data_memory_link)
	{
		output->fatal(CALL_INFO, -1, "Error loading memory interface module.\n");
	}
	else
	{
		output->verbose(CALL_INFO, 1, 0, "Loaded memory interface successfully.\n");
	}

	success=data_memory_link->initialize("data_memory_link", data_handler );
	if(success)
	{
		output->verbose(CALL_INFO, 1, 0, "Loaded memory initialize routine returned successfully.\n");
	}
	else
	{
		output->fatal(CALL_INFO, -1, "Failed to initialize interface: %s\n", "memHierarchy.memInterface");
	}
	if(!data_memory_link)
	{
		output->fatal(CALL_INFO, -1, "Error loading memory interface module.\n");
	}
	else
	{
		output->verbose(CALL_INFO, 1, 0, "Loaded memory interface successfully.\n");
	}

//	instruction_memory_link = dynamic_cast<SST::Interfaces::SimpleMem*>(Super::loadModuleWithComponent("memHierarchy.memInterface", this, params));
//	SST::Interfaces::SimpleMem::Handler<mips_core> *instruction_handler=new SST::Interfaces::SimpleMem::Handler<mips_core>(this,&mips_core::instruction_memory_callback);
//	success=instruction_memory_link->initialize("instruction_memory_link", instruction_handler );
//	if(!success) std::cout<<"Error initialising the instruction_memory_link"<<std::endl;

	output->verbose(CALL_INFO, 1, 0, "Configuration of memory interface completed.\n");


	program=params.find<std::string>("input_file","");
	latencies=params.find<std::string>("configuration_file","");
	output_file=params.find<std::string>("output_file","./out.json");


	// tell the simulator not to end without us
	registerAsPrimaryComponent();
	primaryComponentDoNotEndSim();



}


void mips_core::setup()
{
	output->output("Setting up.\n");

	Super::registerExit();

	// Create a tick function with the frequency specified
	Super::registerClock( clock_frequency, new Clock::Handler<mips_core>(this, &mips_core::tick ) );

	memory_latency = new SSTMemory(data_memory_link);
	state = new implementation::state::ProcessorState(*memory_latency);
	core = new implementation::mips_core_simulator::XMIPSCore(program, latencies, output_file, *state);

}


void mips_core::init(unsigned int phase)
{
	output->output("Initializing.\n");
	if ( !phase ) {
//		data_memory_link->sendInitData(new Interfaces::StringEvent("SST::MemHierarchy::MemEvent"));
//		instruction_memory_link->sendInitData(new Interfaces::StringEvent("SST::MemHierarchy::MemEvent"));
	}
}

void mips_core::finish()
{
/*	std::cout<<"Finishing"<<std::endl;
	delete core;*/
}

bool mips_core::tick(Cycle_t cycle)
{
	output->verbose(CALL_INFO, 1, 0, "Tick.\n");
	if(!core->run_cycle())
	{
		primaryComponentOKToEndSim();
		unregisterExit();
		return true;
	}
	return false;
}

void mips_core::instruction_memory_callback(Interfaces::SimpleMem::Request *ev)
{

}

void mips_core::memory_callback(Interfaces::SimpleMem::Request *ev)
{
	if(memory_latency)
	{
		memory_latency->callback(ev);
	}
}

}
}
