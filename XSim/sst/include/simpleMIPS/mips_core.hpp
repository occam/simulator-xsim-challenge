#ifndef __MIPS_CORE_HPP__
#define __MIPS_CORE_HPP__

#include <sst/core/component.h>
#include <sst/core/sst_types.h>
#include <sst/core/output.h>
#include <sst/core/interfaces/simpleMem.h>

#include <mips_core_simulator/core.hpp>
#include<simpleMIPS/sst_memory.hpp>

namespace SST
{
namespace simpleMIPS
{

class mips_core :
		public SST::Component
{
	private:
		using Super=SST::Component;

	public:

		mips_core(SST::ComponentId_t id, SST::Params& params);

		virtual void init(unsigned int phase) override final;
		virtual void setup() override final;
		virtual void finish() override final;
	private:
		mips_core();
		mips_core(const mips_core& params);
		mips_core operator=(const mips_core& params);
		virtual ~mips_core() = default;

	protected:
		bool tick(Cycle_t cycle);
		void instruction_memory_callback(Interfaces::SimpleMem::Request *ev);
		void memory_callback(SST::Interfaces::SimpleMem::Request *ev);

	private:
		std::string clock_frequency="0Hz";
		int verbose=0;
		std::set<uint64_t> transactions_pending;

		SST::Output *output=nullptr;
		SST::Interfaces::SimpleMem *data_memory_link=nullptr;
		SST::Interfaces::SimpleMem *instruction_memory_link=nullptr;

		std::string program;
		std::string latencies;
		std::string output_file;

		SSTMemory *memory_latency=nullptr;
		implementation::state::ProcessorState *state=nullptr;
		implementation::mips_core_simulator::XMIPSCore *core=nullptr;

};

}
}

#endif
