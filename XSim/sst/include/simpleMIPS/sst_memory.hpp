#pragma once
#include <config/arch_config.hpp>
#include <sst/core/interfaces/simpleMem.h>
#include<zero_latency_memory.hpp>
#include <functional>


class SSTMemory:
		public MemoryLatencyI
{

	public:
		SSTMemory(SST::Interfaces::SimpleMem *data_memory_link):
			data_memory_link(data_memory_link)
		{

		}

		SSTMemory(const SSTMemory &other) = default;
		SSTMemory(SSTMemory &&other) = default;

		SSTMemory &operator=(const SSTMemory &other) = default;
		SSTMemory &operator=(SSTMemory &&other) = default;

		virtual ~SSTMemory() noexcept = default;

		void callback(SST::Interfaces::SimpleMem::Request *ev)
		{
			try{
				callbacks.at(ev->id)(ev->id,ev->addr);
			}
			catch(std::exception &e)
			{
				std::cout<<"No such callback"<<std::endl;
				exit(EXIT_FAILURE);
			}

		}

		virtual void read(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
		{
			SST::Interfaces::SimpleMem::Request *req=new SST::Interfaces::SimpleMem::Request(
						//Command
						SST::Interfaces::SimpleMem::Request::Command::Read,
						//uint64 addr
						address,
						//uint64 size
						sizeof(uarch_t),
						//flags_t
						0,
						//flags_t
						0
						);
			callbacks.insert({req->id, cb});
			data_memory_link->sendRequest(req);
		}

		virtual void write(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
		{
			SST::Interfaces::SimpleMem::Request *req=new SST::Interfaces::SimpleMem::Request(
						//Command
						SST::Interfaces::SimpleMem::Request::Command::Write,
						//uint64 addr
						address,
						//uint64 size
						sizeof(uarch_t),
						//flags_t
						0,
						//flags_t
						0
						);
			callbacks.insert({req->id, cb});
			data_memory_link->sendRequest(req);
		}

	protected:
		std::map<uint64_t, std::function<void(uarch_t, uarch_t)> > callbacks;
		SST::Interfaces::SimpleMem *data_memory_link=nullptr;
	private:

};
