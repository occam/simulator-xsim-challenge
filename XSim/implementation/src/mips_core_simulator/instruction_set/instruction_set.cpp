#include <mips_core_simulator/instruction_set/instruction_set.hpp>

/** Include instructions here **/
#include <mips_core_simulator/instruction_set/instructions/add.hpp>
#include <mips_core_simulator/instruction_set/instructions/sub.hpp>
#include <mips_core_simulator/instruction_set/instructions/and.hpp>
#include <mips_core_simulator/instruction_set/instructions/nor.hpp>
#include <mips_core_simulator/instruction_set/instructions/div.hpp>
#include <mips_core_simulator/instruction_set/instructions/mul.hpp>
#include <mips_core_simulator/instruction_set/instructions/mod.hpp>
#include <mips_core_simulator/instruction_set/instructions/exp.hpp>
#include <mips_core_simulator/instruction_set/instructions/lw.hpp>
#include <mips_core_simulator/instruction_set/instructions/sw.hpp>
#include <mips_core_simulator/instruction_set/instructions/liz.hpp>
#include <mips_core_simulator/instruction_set/instructions/lis.hpp>
#include <mips_core_simulator/instruction_set/instructions/lui.hpp>
#include <mips_core_simulator/instruction_set/instructions/bp.hpp>
#include <mips_core_simulator/instruction_set/instructions/bn.hpp>
#include <mips_core_simulator/instruction_set/instructions/bx.hpp>
#include <mips_core_simulator/instruction_set/instructions/bz.hpp>
#include <mips_core_simulator/instruction_set/instructions/jr.hpp>
#include <mips_core_simulator/instruction_set/instructions/jalr.hpp>
#include <mips_core_simulator/instruction_set/instructions/j.hpp>
#include <mips_core_simulator/instruction_set/instructions/halt.hpp>
#include <mips_core_simulator/instruction_set/instructions/put.hpp>
#include <mips_core_simulator/instruction_set/instructions/nop.hpp>

#include <iostream>
#include <algorithm>
#include <json/json.h>



#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<(msg)<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif
namespace implementation
{
namespace mips_core_simulator
{

InstructionSet::InstructionSet(std::string latencies_config_path)
{
	load_instruction_set(load_latencies(latencies_config_path));
	std::cout<<*this<<std::endl;
}

InstructionSet::~InstructionSet()
{
	instruction_factory.clear();
}

InstructionSet::GenericInstruction *InstructionSet::get_instruction(InstructionSet::RawType_t r_instruction)
{
	try
	{
		return instruction_factory.at(r_instruction.opcode())(r_instruction);
	}
	catch (std::out_of_range &e)
	{
		// nop
		return instruction_factory.at(255)(r_instruction);
	}
}

std::string InstructionSet::get_name(RawType_t instruction) const
{
	RawType_t r_instruction(instruction);
	try
	{
		return instruction_names.at(r_instruction.opcode());
	}
	catch (std::out_of_range &e)
	{

		return "nop";
	}
}

std::ostream &operator<<(std::ostream &out, const InstructionSet &set)
{
	for(const std::pair<InstructionSet::uarch_t, InstructionSet::Factory_t> pair:set.instruction_factory)
	{
		InstructionSet::GenericInstruction *gi=pair.second(0);
		out<<*gi<<std::endl;
		delete gi;
	}
	return out;

}

#define ADD_INSTRUCTION(name, class_name) 	{\
	opname=(name);\
	latency = latencies.find(opname)!=latencies.end()?latencies.at(opname):0;\
	InstructionSet::Factory_t function=[latency](RawType_t instruction)\
	{\
		return new implementation::mips_core_simulator::instructions::class_name (instruction,latency);\
	};\
	instruction = function(0);\
	instruction_factory.insert({instruction->get_opcode(), function});\
	instruction_names.insert({instruction->get_opcode(), instruction->get_name()});\
	PDEBUG(*instruction);\
	delete instruction;\
	instruction=nullptr;\
}

void InstructionSet::load_instruction_set(std::map<std::string, InstructionSet::uarch_t> latencies)
{
	std::string opname="";
	GenericInstruction *instruction=nullptr;
	uarch_t latency = 0;

	ADD_INSTRUCTION("add",Add);
	ADD_INSTRUCTION("sub",Sub);
	ADD_INSTRUCTION("and",And);
	ADD_INSTRUCTION("nor",Nor);
	ADD_INSTRUCTION("div",Div);
	ADD_INSTRUCTION("mul",Mul);
	ADD_INSTRUCTION("mod",Mod);
	ADD_INSTRUCTION("exp",Exp);
	ADD_INSTRUCTION("lw",Lw);
	ADD_INSTRUCTION("sw",Sw);
	ADD_INSTRUCTION("liz",Liz);
	ADD_INSTRUCTION("lis",Lis);
	ADD_INSTRUCTION("lui",Lui);
	ADD_INSTRUCTION("bp",Bp);
	ADD_INSTRUCTION("bn",Bn);
	ADD_INSTRUCTION("bx",Bx);
	ADD_INSTRUCTION("bz",Bz);
	ADD_INSTRUCTION("jr",Jr);
	ADD_INSTRUCTION("jarl",Jalr);
	ADD_INSTRUCTION("j",J);
	ADD_INSTRUCTION("halt",Halt);
	ADD_INSTRUCTION("put",Put);
	ADD_INSTRUCTION("nop",Nop);

}

std::map<std::string, InstructionSet::uarch_t> InstructionSet::load_latencies(std::string config_path)
{
	std::map<std::string, InstructionSet::uarch_t> ret;

	Json::Reader jsonReader;
	Json::Value jsonRoot;
	bool validRoot;

	std::ifstream configuration(config_path, std::ifstream::binary);

	validRoot=jsonReader.parse(configuration,jsonRoot);
	if (!validRoot)
	{
		std::cout  << "Failed to parse latencies\n"
				   << jsonReader.getFormattedErrorMessages();
	}

	std::cout << "Loading configuration"<<std::endl;
	for( Json::ValueIterator itr = jsonRoot.begin() ; itr != jsonRoot.end() ; itr++ )
	{
		std::string opname=itr.key().asString();
		std::size_t latency=jsonRoot[opname].asInt64();
		try
		{
			std::transform(opname.begin(), opname.end(), opname.begin(), tolower);
			ret.insert({opname,latency});
			//			std::cout<<opname<<": "<<latency<<std::endl;
		}
		catch(std::out_of_range &e)
		{
			std::cout << "could not find instruction " << opname << std::endl;
		}
	}
	return ret;

}




}
}
