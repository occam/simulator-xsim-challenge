#include <mips_core_simulator/instruction_set/instruction_types/ix_type_t.hpp>
#include <sstream>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{
IXType_t::IXType_t(const RawType_t &other):
	RawType_t(other)
{
}

IXType_t::IXType_t(RawType_t &&other):
	RawType_t(other)
{
}

IXType_t::uarch_t IXType_t::imm11() const
{
	return get_range<0,10>();
}


std::string IXType_t::to_string() const
{
	std::stringstream ss;
	ss<<"IX op("<<opcode()<<"), imm11 = "<<imm11();
	return ss.str();
}


std::ostream& operator<<(std::ostream& out, const IXType_t& inst)
{
	out<<inst.to_string();
	return out;
}

}
}
}
