#include <mips_core_simulator/instruction_set/instruction_types/r_type_t.hpp>
#include <sstream>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{

RType_t::RType_t(const RawType_t &other):
	RawType_t(other)
{
}

RType_t::RType_t(RawType_t &&other):
	RawType_t(other)
{
}

RType_t::uarch_t RType_t::rd() const
{
	return get_range<8,10>();
}

RType_t::uarch_t RType_t::rs() const
{
	return get_range<5,7>();
}

RType_t::uarch_t RType_t::rt() const
{
	return get_range<2,4>();
}

std::string RType_t::to_string() const
{
	std::stringstream ss;
	ss<<"R op("<<opcode()<<"), r"<<rd()<<", r"<<rs()<<", r"<<rt();
	return ss.str();
}

std::ostream& operator<<(std::ostream& out, const RType_t& inst)
{
	out<<inst.to_string();
	return out;
}

}
}
}
