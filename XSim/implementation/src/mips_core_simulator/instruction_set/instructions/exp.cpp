#include <mips_core_simulator/instruction_set/instructions/exp.hpp>
#include <cmath>
#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

Exp::Exp(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(20):(latency);
	opcode=7;
	type="R";
	name="exp";
	this->raw_instruction=raw_instruction;
}

bool Exp::run(ProcessorState &processor_state)
{
	RType_t instruction(raw_instruction);
	uarch_t &rd=processor_state.get_register(instruction.rd());
	uarch_t &rs=processor_state.get_register(instruction.rs());
	uarch_t &rt=processor_state.get_register(instruction.rt());

	cycles_spent++;
	if(cycles_spent>=latency)
	{
		rd=(uarch_t)std::pow(rs,rt);
		processor_state.increment_pc();
		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}
