#include <mips_core_simulator/instruction_set/instructions/mul.hpp>

#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

Mul::Mul(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(10):(latency);
	opcode=5;
	type="R";
	name="mul";
	this->raw_instruction=raw_instruction;
}

bool Mul::run(ProcessorState &processor_state)
{
	RType_t instruction(raw_instruction);
	uarch_t &rd=processor_state.get_register(instruction.rd());
	uarch_t &rs=processor_state.get_register(instruction.rs());
	uarch_t &rt=processor_state.get_register(instruction.rt());

	cycles_spent++;
	if(cycles_spent>=latency)
	{
		rd=rs*rt;
		processor_state.increment_pc();
		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}
