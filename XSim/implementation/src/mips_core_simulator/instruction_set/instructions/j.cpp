#include <mips_core_simulator/instruction_set/instructions/j.hpp>

#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

J::J(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(1):(latency);
	opcode=24;
	type="IX";
	name="j";
	this->raw_instruction=raw_instruction;
}

bool J::run(ProcessorState &processor_state)
{
	IXType_t instruction(raw_instruction);
	uarch_t imm11=instruction.imm11();
	uarch_t &pc = processor_state.get_pc();

	cycles_spent++;
	if(cycles_spent>=latency)
	{

		pc = imm11&0x07FF;

		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}

