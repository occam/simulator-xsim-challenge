#include <mips_core_simulator/instruction_set/instructions/lui.hpp>

#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

Lui::Lui(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(1):(latency);
	opcode=18;
	type="I";
	name="lui";
	this->raw_instruction=raw_instruction;
}

bool Lui::run(ProcessorState &processor_state)
{
	IType_t instruction(raw_instruction);
	uint16_t uimm16=instruction.imm8();
	uarch_t &rd=processor_state.get_register(instruction.rd());

	cycles_spent++;
	if(cycles_spent>=latency)
	{
		rd&=0x00FF;
		rd|=uimm16<<8;
		processor_state.increment_pc();
		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}

