#include <mips_core_simulator/memory/memory.hpp>
#include <iostream>
#include <config/arch_config.hpp>
#include <exception>


#ifdef DEBUG
	#ifdef MEMORY_DEBUG
		#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
	#else
		#define PDEBUG(msg)
	#endif
#else
	#define PDEBUG(msg)
#endif


namespace implementation
{

namespace memory
{
uint8_t Memory::get(uarch_t address) const
{
	uint8_t data;
	try
	{
		PDEBUG("Reading from address "<<address);
		data=memory.at(address);
	}
	catch(const std::out_of_range &e)
	{
		PERROR("Trying to access invalid address : "<<address);
		throw e;
	}
	return data;
}

void Memory::set(uarch_t address, uint8_t data)
{
	try
	{
		PDEBUG("Writting to address "<<address);
		memory.at(address)=data;
	}
	catch(const std::out_of_range &e)
	{
		PERROR("Trying to access invalid address: "<<address);
		throw e;
	}
}


uarch_t Memory::getw(uarch_t address) const
{
	address<<=1;
	uarch_t word(0);
	try
	{
		word = get(address) << 8;
		word &= 0xFF00;
		word |= get(address+1);
	}
	catch(const std::out_of_range &e)
	{
		throw e;
	}
	return word;
}

void Memory::setw(uarch_t address, uarch_t data)
{
	address<<=1;
	try
	{
		set(address,(data>>8)&0x00FF);
		set(address+1,data&0x00FF);
	}
	catch(const std::out_of_range &e)
	{
		throw e;
	}
}

}
}

