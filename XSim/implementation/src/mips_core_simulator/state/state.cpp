#include <mips_core_simulator/state/state.hpp>
#include <sstream>
namespace implementation
{
namespace state
{

ProcessorState::ProcessorState(MemoryLatencyI &latency_simulator):
	data_memory_latency(latency_simulator)
{
}

void ProcessorState::increment_pc()
{
	pc+=1;
}

void ProcessorState::set_pc(const arch::uarch_t &pc)
{
	this->pc=pc;
}

void ProcessorState::get_pc(arch::uarch_t &pc)
{
	pc=this->pc;
}

const arch::uarch_t &ProcessorState::get_pc() const
{
	return pc;
}

arch::uarch_t &ProcessorState::get_pc()
{
	return pc;
}

const ProcessorState::register_t &ProcessorState::get_registers() const
{
	return registers;
}

ProcessorState::register_t &ProcessorState::get_registers()
{
	return registers;
}

const ProcessorState::uarch_t &ProcessorState::get_register(uarch_t n) const
{
	return registers.at(n);
}

ProcessorState::uarch_t &ProcessorState::get_register(uarch_t n)
{
	return registers.at(n);
}

const ProcessorState::Memory &ProcessorState::get_instruction_memory() const
{
	return instruction_memory;
}

ProcessorState::Memory &ProcessorState::get_instruction_memory()
{
	return instruction_memory;
}

const ProcessorState::Memory &ProcessorState::get_data_memory() const
{
	return data_memory;
}

ProcessorState::Memory &ProcessorState::get_data_memory()
{
	return data_memory;
}

void ProcessorState::simulate_data_memory_read(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
{
	data_memory_access_in_progess=true;
	data_memory_latency.read(id, address, [this, cb](uarch_t id, uarch_t address)
	{
		data_memory_access_in_progess=false;
		cb(id, address);
	});
}

bool ProcessorState::is_data_memory_busy() const
{
	return data_memory_access_in_progess;
}

void ProcessorState::simulate_data_memory_write(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
{
	data_memory_access_in_progess=true;
	data_memory_latency.write(id, address, [this, cb](uarch_t id, uarch_t address)
	{
		data_memory_access_in_progess=false;
		cb(id, address);
	});
}

std::string ProcessorState::to_string() const
{
	std::stringstream ss;
	ss<<"pc: "<<std::setfill('0')<<std::setw(4)<<pc<<" :: ";
	int i=0;
	for(auto val:registers)
	{
		ss<<"r"<<i++<<": "<<std::setfill('0')<<std::setw(4)<<val<<", ";
	}
	return ss.str();
}

std::ostream &operator<<(std::ostream &out, const ProcessorState &state)
{
	out<<state.to_string();
	return out;
}


}
}
