#pragma once

#include <config/arch_config.hpp>

#include <mips_core_simulator/state/state.hpp>

#include <mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>
#include <mips_core_simulator/instruction_set/instruction_set.hpp>
#include <array>
#include <vector>
#include <functional>
#include <map>
#include <sstream>
#include <mips_core_simulator/memory_interface/memory_interface.hpp>



namespace implementation
{
namespace mips_core_simulator
{

class XMIPSCore
{
		using ProcessorState=state::ProcessorState;
		using uarch_t=arch::uarch_t;
		using IType_t=implementation::mips_core_simulator::instruction_types::IType_t;
		using IXType_t=implementation::mips_core_simulator::instruction_types::IXType_t;
		using RType_t=implementation::mips_core_simulator::instruction_types::RType_t;
		using RawType_t=implementation::mips_core_simulator::instruction_types::RawType_t;
		using MemoryI = implementation::mips_core_simulator::memory_interface::MemoryI;
		using GenericInstruction=implementation::mips_core_simulator::instructions::GenericInstruction;

	public:
		XMIPSCore()=delete;
		XMIPSCore(std::string program, std::string latencies, std::string output, ProcessorState &state);

		XMIPSCore(const XMIPSCore &other) = default;
		XMIPSCore(XMIPSCore &&other) = default;

		XMIPSCore &operator=(const XMIPSCore &other) = default;
		XMIPSCore &operator=(XMIPSCore &&other) = default;

		virtual ~XMIPSCore() noexcept = default;

		// Function that executes at each clock cycle
		bool run_cycle();

		// Execution manager
		bool add_execution(RawType_t *instruction_word, GenericInstruction *g_instruction);
		bool tick();

	protected:
		// Loads the program into instruction memory
		void program_loader(std::string path);
		// Outputs the simulation log
		void write_log();
	protected:
		// The CPU instruction set implementation
		InstructionSet instruction_set;
		// The state of the processor (memory+registers+pc)
		ProcessorState &state;


	private:

		RawType_t *r_instruction=nullptr;

		class ExecutionUnit
		{
			public:
				GenericInstruction *unit=nullptr;
				RawType_t *r_instruction=nullptr;
		};
		ExecutionUnit execution_unit;
		uarch_t cycles_busy=0;
		bool first_time=true;

		std::stringstream trace;
		std::map<std::string,std::size_t> instruction_count;
		std::uint64_t cycle_count=0;
		std::string output;

		bool terminate=false;
};

}
}

