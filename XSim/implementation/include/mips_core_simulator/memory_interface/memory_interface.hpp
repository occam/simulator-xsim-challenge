#pragma once

#include <config/arch_config.hpp>

namespace implementation
{
namespace mips_core_simulator
{
namespace memory_interface
{

class MemoryI
{
	public:
		using uarch_t=implementation::arch::uarch_t;

	public:
		MemoryI() = default;

		MemoryI(const MemoryI &other) = default;
		MemoryI(MemoryI &&other) = default;

		MemoryI &operator=(const MemoryI &other) = default;
		MemoryI &operator=(MemoryI &&other) = default;

		virtual ~MemoryI() noexcept = default;

		virtual void getw(uarch_t address, std::function<void(uarch_t)> cb) const = 0;
		virtual void setw(uarch_t address, uarch_t data, std::function<void(void)> cb) = 0;

	protected:

	private:

};

}
}
}
