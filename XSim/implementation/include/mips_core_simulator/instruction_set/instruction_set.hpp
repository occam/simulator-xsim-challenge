#ifndef __INSTRUCTION_SET_HPP__
#define  __INSTRUCTION_SET_HPP__

#include <map>
#include <functional>
#include <fstream>
#include <mips_core_simulator/instruction_set/instructions/generic_instruction.hpp>



namespace implementation
{
namespace mips_core_simulator
{


class InstructionSet
{
	public:
		using GenericInstruction=implementation::mips_core_simulator::instructions::GenericInstruction;
		using uarch_t=arch::uarch_t;
		using ProcessorState=implementation::state::ProcessorState;
		using RawType_t=implementation::mips_core_simulator::instruction_types::RawType_t;

	public:
		InstructionSet() = delete;
		InstructionSet(std::string latencies_config_path);

		InstructionSet(const InstructionSet &other) = default;
		InstructionSet(InstructionSet &&other) = default;

		InstructionSet &operator=(const InstructionSet &other) = default;
		InstructionSet &operator=(InstructionSet &&other) = default;

		virtual ~InstructionSet() noexcept;

		GenericInstruction *get_instruction(RawType_t r_instruction);
		std::string get_name(RawType_t instruction) const;

	protected:

		using Factory_t= std::function<GenericInstruction *(RawType_t r_instruction)> ;
		// Map opcode => factory
		std::map<uarch_t, Factory_t > instruction_factory;
		// Map opcode => name
		std::map<uarch_t, std::string > instruction_names;

		friend std::ostream& operator<<(std::ostream& out, const InstructionSet& set);

	private:


		void load_instruction_set(std::map<std::string, uarch_t> latencies);
		std::map<std::string, uarch_t> load_latencies(std::string config_path);
};

std::ostream& operator<<(std::ostream& out, const InstructionSet& set);


}
}

#endif
