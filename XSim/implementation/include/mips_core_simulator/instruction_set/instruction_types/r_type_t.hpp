#ifndef __R_TYPE_T_HPP__
#define __R_TYPE_T_HPP__

#include <mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{
class RType_t:
		public RawType_t
{

	public:
		RType_t() = default;

		RType_t(const RType_t &other) = default;
		RType_t(RType_t &&other) = default;

		RType_t(const RawType_t &other);
		RType_t(RawType_t &&other);

		RType_t &operator=(const RType_t &other) = default;
		RType_t &operator=(RType_t &&other) = default;

		virtual ~RType_t() noexcept = default;

		uarch_t rd() const;
		uarch_t rs() const;
		uarch_t rt() const;

	protected:
		virtual std::string to_string() const override;

	private:
		friend std::ostream& operator<<(std::ostream& out, const RType_t& inst);

};
std::ostream& operator<<(std::ostream& out, const RType_t& inst);


}
}
}

#endif
