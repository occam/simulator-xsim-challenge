#ifndef __RAW_TYPE_T_HPP__
#define __RAW_TYPE_T_HPP__

#include <config/arch_config.hpp>
#include <fstream>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{


class RawType_t:
		public std::bitset<sizeof(arch::uarch_t)*8>
{
	protected:
		using uarch_t=arch::uarch_t;
	private:
	    using Super=std::bitset<sizeof(uarch_t)*8>;
	public:
	    RawType_t();
		RawType_t(uarch_t instruction);

		RawType_t(const RawType_t &other) = default;
		RawType_t(RawType_t &&other) = default;

		RawType_t &operator=(const RawType_t &other) = default;
		RawType_t &operator=(RawType_t &&other) = default;

		virtual ~RawType_t() noexcept = default;



		uarch_t opcode() const;
		uarch_t instruction() const;

	protected:

		virtual std::string to_string() const;

		template<std::size_t Begin, std::size_t End, std::size_t N=sizeof(uarch_t)*8>
		uarch_t get_range() const
		{
			static_assert(Begin <= End, "Invalid range: Begin cannot be greater than End");
			static_assert(End <= N, "Invalid range: End cannot be greater than size of original");
			std::bitset<sizeof(uarch_t)*8> b_set(*this);
			b_set <<= (N - End - 1);
			b_set >>= (N - End - 1 + Begin);
			return b_set.to_ulong();
		}


	private:
		friend std::ostream& operator<<(std::ostream& out, const RawType_t& inst);

};
std::ostream& operator<<(std::ostream& out, const RawType_t& inst);

}
}
}

#endif
