#pragma once

#include <mips_core_simulator/instruction_set/instructions/generic_instruction.hpp>

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

class Bz:
		public GenericInstruction
{
	public:

		Bz(const RawType_t raw_instruction, uarch_t latency);

		bool run(ProcessorState &processor_state) override;
};



}
}
}

