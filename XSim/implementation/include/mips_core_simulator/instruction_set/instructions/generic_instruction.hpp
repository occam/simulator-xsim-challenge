#pragma once


#include<mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>
#include<mips_core_simulator/instruction_set/instruction_types/r_type_t.hpp>
#include<mips_core_simulator/instruction_set/instruction_types/ix_type_t.hpp>
#include<mips_core_simulator/instruction_set/instruction_types/i_type_t.hpp>
#include <config/arch_config.hpp>
#include<mips_core_simulator/memory/memory.hpp>
#include <mips_core_simulator/state/state.hpp>
namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{


class GenericInstruction
{
	protected:
		using RawType_t=mips_core_simulator::instruction_types::RawType_t;
		using RType_t=mips_core_simulator::instruction_types::RType_t;
		using IType_t=mips_core_simulator::instruction_types::IType_t;
		using IXType_t=mips_core_simulator::instruction_types::IXType_t;
		using uarch_t=implementation::arch::uarch_t;
		using arch_t=implementation::arch::sarch_t;
		using ProcessorState=implementation::state::ProcessorState;

	public:

		virtual ~GenericInstruction() = default;
		virtual bool run(ProcessorState &processor_state) = 0;

		std::string get_type() const;
		std::string get_name() const;
		uarch_t get_latency() const;
		uarch_t get_opcode() const;

	protected:
		uarch_t latency=1;
		uarch_t cycles_spent=0;
		uarch_t opcode=-1;
		std::string type="?";
		std::string name="";
		RawType_t raw_instruction;
		friend std::ostream& operator<<(std::ostream& out, const GenericInstruction& set);

};

std::ostream& operator<<(std::ostream& out, const GenericInstruction& set);


}
}
}

