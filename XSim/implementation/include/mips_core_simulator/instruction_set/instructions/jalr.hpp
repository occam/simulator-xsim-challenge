#pragma once

#include <mips_core_simulator/instruction_set/instructions/generic_instruction.hpp>

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

class Jalr:
		public GenericInstruction
{
	public:

		Jalr(const RawType_t raw_instruction, uarch_t latency);

		bool run(ProcessorState &processor_state) override;
};



}
}
}

