#ifndef __ARCH_CONFIG_HPP__
#define __ARCH_CONFIG_HPP__

#include <cstdint>
#include <functional>
#include <bitset>
#include <arpa/inet.h>
#include <cstring>
#include <iostream>
#include <iomanip>

//#define DEBUG
//#define IS_DEBUG
//#define MEMORY_DEBUG
#define CORE_DEBUG
#define PAUSE_DEBUG


#define PERROR(error) std::cerr<<"Error("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<error<<std::endl


namespace implementation
{

namespace arch
{

// Set architecture word size
using uarch_t=std::uint16_t;
using sarch_t=std::int16_t;


// Instruction types
enum InstructionType
{
	TYPE_R=0,
	TYPE_I,
	TYPE_IX
};



}
}

#endif
