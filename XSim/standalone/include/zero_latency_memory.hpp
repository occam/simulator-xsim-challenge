#pragma once
#include <config/arch_config.hpp>
#include <functional>
class MemoryLatencyI
{

	public:
		MemoryLatencyI() = default;

		MemoryLatencyI(const MemoryLatencyI &other) = default;
		MemoryLatencyI(MemoryLatencyI &&other) = default;

		MemoryLatencyI &operator=(const MemoryLatencyI &other) = default;
		MemoryLatencyI &operator=(MemoryLatencyI &&other) = default;

		virtual ~MemoryLatencyI() noexcept = default;
	public:
		using uarch_t = implementation::arch::uarch_t;
	public:
		virtual void read(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb) = 0;
		virtual void write(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb) = 0;

	protected:

	private:

};

class ZeroLatencyMemory:
		public MemoryLatencyI
{

	public:
		ZeroLatencyMemory() = default;

		ZeroLatencyMemory(const ZeroLatencyMemory &other) = default;
		ZeroLatencyMemory(ZeroLatencyMemory &&other) = default;

		ZeroLatencyMemory &operator=(const ZeroLatencyMemory &other) = default;
		ZeroLatencyMemory &operator=(ZeroLatencyMemory &&other) = default;

		virtual ~ZeroLatencyMemory() noexcept = default;

		virtual void read(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
		{
			cb(id,address);
		}

		virtual void write(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb)
		{
			cb(id,address);
		}

	protected:

	private:

};
