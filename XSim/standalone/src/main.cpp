#include <iostream>
#include <string>
#include <mips_core_simulator/core.hpp>
#include <mips_core_simulator/state/state.hpp>

#include <zero_latency_memory.hpp>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc!=4)
	{
		std::cout << "Wrong usage:"<<std::endl<<
		             "\t"<<	argv[0]<<" inputfile configfile outputstatfile"<<std::endl<<
		             "\t"<<"\t"<<"inputfile - program to run in this processor"<<std::endl<<
		             "\t"<<"\t"<<"configfile - json file with the instruction latencies"<<std::endl<<
		             "\t"<<"\t"<<"outputstatfile - file where output will be written"<<std::endl;
		return -1;
	}

	std::string program=argv[1];
	std::string latencies=argv[2];
	std::string output=argv[3];

	double timestep=1.0/8000000.0;

	ZeroLatencyMemory memory_latency;
	implementation::state::ProcessorState state(memory_latency);
	implementation::mips_core_simulator::XMIPSCore core(program, latencies, output, state);


	while(core.run_cycle());


	return 0;
}
